---
title: About
hideMeta: true
---

# 👋&nbsp; Hello, my name is Brett.

# I live and work in San Francisco and I'm keen on graphics, data and design.

---
title: Leap Motion Controller Python SDK on Apple M1
date: 2021-01-20
---

Leap's macOS Python SDK releases have fallen behind, so there is a bit of jiggery needed
to get the latest version (2.3.1 as of this post) working on the Apple M1 chip.

1. Download the [Leap Motion SDK](https://developer.leapmotion.com/sdk-leap-motion-controller/)
2. Install the included client Leap Motion.pkg (this will also install the debug visualizer).
3. Frameworks have been moved from `/Library/Frameworks` to `/System/Library/Frameworks` in the most recent macOS releases, so let's help Leap find it with a symlink.

```bash
sudo ln -s /System/Library/Frameworks/Python.framework /Library/Frameworks/Python.framework
```

4. Create a Rosetta version of "Terminal.app". Duplicate "Terminal.app" in `/System/Applications/Utilities` and name the copy "Rosetta Terminal.app".  Use the Finder to File > Get Info on the Rosetta Terminal and check "Open using Rosetta".  Now when executing within this special terminal, Python will be directed to properly read the Intel built shared library "LeapPython.so" instead of trying and failing to find an ARM build.

5. Test things out by copying the following from the LeapSDK directory (included with the download) into a temporary directory:
    * `LeapSDK/lib/Leap.py`
    * `LeapSDK/lib/LeapPython.so`
    * `LeapSDK/lib/libLeap.dylib`
    * `LeapSDK/samples/Sample.py`

And running the Sample script using legacy `python2`:
```bash
python2 Sample.py
```

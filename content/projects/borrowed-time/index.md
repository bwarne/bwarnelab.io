---
title: Borrowed Time
date: 2015-10-31
thumbnail: /projects/borrowed-time/img/houdini.png
hideMeta: true
---

![cover](img/bt070_119k.light_comp_render.135.png)

> A weathered Sheriff returns to the remains of an accident he has spent a lifetime trying to forget. With each step forward, the memories come flooding back. Faced with his mistake once again, he must find the strength to carry on.

An Oscar nominated animated short film released in 2015 that I developed a number of dust effects for. Simulation was performed in Houdini with rendering in RenderMan and compositing in Nuke.

<img align="right" src="img/development.gif">

It was an increadible opportunity to work alongside talented artists who were putting in long nights and weekends to make this project come together.

Watch the [trailer](https://vimeo.com/ondemand/100733/136345350?autoplay=1) or spot me in the [behind the scenes](https://vimeo.com/ondemand/100733/195986943) film.

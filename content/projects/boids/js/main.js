// import * as THREE from 'https://threejsfundamentals.org/threejs/resources/threejs/r122/build/three.module.js';
// import {OrbitControls} from 'https://threejsfundamentals.org/threejs/resources/threejs/r122/examples/jsm/controls/OrbitControls.js';
// import {GUI} from 'https://threejsfundamentals.org/threejs/../3rdparty/dat.gui.module.js';


// import * as THREE from './build/three.module.js';
// import * as THREE from 'https://cdnjs.cloudflare.com/ajax/libs/three.js/r120/three.module.js';
// import * as THREE from './build/three.module.js';
import * as THREE from '../../../js/three/build/three.module.js';

import { MeshLine, MeshLineMaterial, MeshLineRaycast } from './ext/THREE.MeshLine.js';

import newtonRaphson from './ext/newtonRaphson.js';

// import { TransformControls } from "./ext/jsm/controls/TransformControls.js";
import { DragControls } from '../../../js/three/ext/jsm/controls/DragControls.js';

// import Stats from './ext/jsm/libs/stats.module.js';
// import { GUI } from './ext/jsm/libs/dat.gui.module.js';

import { GPUComputationRenderer } from '../../../js/three/ext/jsm/misc/GPUComputationRenderer.js';

// import shader strings
import fragmentShaderPosition from './glsl/position.frag.js';
import fragmentShaderVelocity from './glsl/velocity.frag.js';
// import fragmentShaderGoal from './glsl/goal.frag.js';
import birdVS from './glsl/bird.vert.js';
import birdFS from './glsl/bird.frag.js';

/* TEXTURE WIDTH FOR SIMULATION */
const WIDTH = 8;

const BIRDS = WIDTH * WIDTH;
const POLE_WIDTH = 16;
const POLE_HEIGHT = 1000;
const WIRE_LENGTH = 220;
const CANVAS_HEIGHT = 600;

function clamp(min, x, max) {
	return Math.max(min, Math.min(x, max));
}

function lerp(t, min, max) {
	return t * (max - min) + min;
}

function getRandomFloat(min, max) {
	return Math.random() * (max - min) + min;
}

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1) + min);
}


// Custom Geometry - using 3 triangles each. No UVs, no normals currently.
function BirdGeometry() {

	const triangles = BIRDS * 3;
	const points = triangles * 3;

	// this context inherits BufferGeometry
	THREE.BufferGeometry.call( this );

	const vertices = new THREE.BufferAttribute( new Float32Array( points * 3 ), 3 );
	const birdColors = new THREE.BufferAttribute( new Float32Array( points * 3 ), 3 );
	const references = new THREE.BufferAttribute( new Float32Array( points * 2 ), 2 );
	const birdVertex = new THREE.BufferAttribute( new Float32Array( points ), 1 );

	this.setAttribute( 'position', vertices );
	this.setAttribute( 'birdColor', birdColors );
	this.setAttribute( 'reference', references );
	this.setAttribute( 'birdVertex', birdVertex );

	// this.setAttribute( 'normal', new Float32Array( points * 3 ), 3 );


	let v = 0;

	// helper function to push points onto vertices BufferAttribute
	function verts_push() {
		for ( let i = 0; i < arguments.length; i ++ ) {
			vertices.array[ v ++ ] = arguments[ i ];
		}
	}

	const wingsSpan = 20;

	for ( let f = 0; f < BIRDS; f ++ ) {

		// Body
		verts_push(
			0, - 0, - 20,
			0, 4, - 20,
			0, 0, 30
		);

		// Left Wing
		verts_push(
			0, 0, - 15,
			- wingsSpan, 0, 0,
			0, 0, 15
		);

		// Right Wing
		verts_push(
			0, 0, 15,
			wingsSpan, 0, 0,
			0, 0, - 15
		);

	}

	// for every vertex in all birds
	for ( let v = 0; v < triangles * 3; v ++ ) {

		const i = ~ ~ ( v / 9 );             	// bitwise floor of positive numbers
		const x = ( i % WIDTH ) / WIDTH;      //
		const y = ~ ~ ( i / WIDTH ) / WIDTH;

		const c = new THREE.Color(
			0x444444 +
			~ ~ ( v / 9 ) / BIRDS * 0x666666
		);

		birdColors.array[ v * 3 + 0 ] = c.r;
		birdColors.array[ v * 3 + 1 ] = c.g;
		birdColors.array[ v * 3 + 2 ] = c.b;

		references.array[ v * 2 ] = x;       // vert in triangle shares x
		references.array[ v * 2 + 1 ] = y;   // vert in triangle shares y

		birdVertex.array[ v ] = v % 9;       // vert offset in bird

	}

	this.scale( 0.2, 0.2, 0.2 );

}

BirdGeometry.prototype = Object.create( THREE.BufferGeometry.prototype );


let container, stats;
let camera, scene, renderer, control;
let mouseX = 0, mouseY = 0;

let windowHalfX = window.innerWidth / 2;
let windowHalfY = window.innerWidth / 4;

const BOUNDS = 800, BOUNDS_HALF = BOUNDS / 2;

let last = performance.now();

let gpuCompute;
let velocityVariable;
let positionVariable;
let goalVariable;
let positionUniforms;
let velocityUniforms;
let birdUniforms;


let numPins = 6;
let pins = [];
let wires = [];
// let targets = Array(BIRDS).fill([0, 0]);
let disturbedBirds = [];

//
// for()





// let dtGoal;

class Wire {
	// numSegments = 20

	constructor(x1, y1, x2, y2, len) {
		const line = new MeshLine();
		const material = new MeshLineMaterial( { color: 0x303030, lineWidth: 3.0 } );
		const mesh = new THREE.Mesh(line, material);

		this.meshLine = line;
		this.mesh = mesh;
		// this.startPin = startPin;
		// this.endPin = endPin;
		this.update(x1, y1, x2, y2, len);
	}
	// catenary

	catenary(x1, y1, x2, y2, length) {
		// https://en.wikipedia.org/wiki/Catenary#Equation
		// Return parameters for catenary curve of form:
		// y = a * cosh((x - b)/a) + c

		// if dist between points is longer than wire
		let d = Math.sqrt((x2-x1)**2 + (y2-y1)**2)
		let s = Math.max(length, d+0.1);

		// solve length of catenary for a
		// 0 = 2*a*sinh((x2 - x1)/(2*a)) - sqrt(s*s - (y2 - y1)**2)
		// using taylor series to expand sinh and solve quadratic for u = a^2
		let ua = 16*(x2 - x1 - Math.sqrt(s*s - (y2-y1)**2))
		let ub = 4*(x2 - x1)**3/6
		let uc = (x2-x1)**5/120
		let u = (-ub - Math.sqrt(ub*ub - 4*ua*uc))/(2*ua)
		let a = Math.sqrt(u)

		// use newton's method to find b where
		// y1 = a * cosh((x1-b)/a) + c
		// y2 = a * cosh((x2-b)/a) + c
		let fb  = b => a*Math.cosh((x2 - b)/a) - a*Math.cosh((x1 - b)/a) - y2 + y1;
		let fbp = b => -Math.sinh((x2 - b)/a) + Math.sinh((x1 - b)/a);
		let b = newtonRaphson(fb, fbp, (x1 + x2)/2, {maxIterations: 50});

		// solve for c
		// y1 = a * cosh((x1-b)/a) + c
		let c = y1 - a * Math.cosh((x1 - b)/a)

		return [a, b, c];
	}

	computeY(x) {
		return this.a * Math.cosh((x - this.b)/this.a) + this.c;
	}

	// parametric position along wire [0,t,1]
	getPosition(t) {
		let x = lerp(t, this.x1, this.x2);
		let y =  this.a * Math.cosh((x - this.b)/this.a) + this.c;
		return [x,y];
	}

	getSlope(t) {
		let x = lerp(t, this.x1, this.x2);
		let y =  Math.sinh((x - this.b)/this.a)
		return y;
	}

	update(x1, y1, x2, y2, len) {
		// const [p1, p2] = [startPin, endPin];
		// let [x1,y1,x2,y2,s] = [p1.position.x, p1.position.y+POLE_HEIGHT/2, p2.position.x, p2.position.y+POLE_HEIGHT/2, 300];
		let [a,b,c] = this.catenary(x1,y1,x2,y2,len);

		// update parameters
		this.x1 = x1;
		this.x2 = x2;
		this.a = a;
		this.b = b;
		this.c = c;

		// update meshline points
			const points = [];
			const step = 5;
			for(let x=x1; x<x2+step; x+=step) {
				x = Math.min(x, x2);
				// let y = a * Math.cosh((x - b)/a) + c
				points.push( new THREE.Vector3(x, this.computeY(x), 0) );
			}

		// const points = [];
		// const n = 5;
		// for(let i=0; i<n; i++) {
		// 	let [x,y] = this.getPoint(i/(n-1));
		// 	points.push( new THREE.Vector3(x,y,0) );
		// }
		this.meshLine.setPoints(points);
	}

}


init();
animate();

function init() {
	// container = document.createElement( 'div' );
	// document.body.appendChild( container );

  const container = document.querySelector('#c');
  // const renderer = new THREE.WebGLRenderer({canvas});

	// let gl = canvas.getContext('webgl');


	camera = new THREE.PerspectiveCamera( 75, 2, 1, 3000 );
	camera.position.z = 250;

	scene = new THREE.Scene();
	scene.background = new THREE.Color( 0xffffff );
	scene.fog = new THREE.Fog( 0xffffff, 100, 1000 );

	renderer = new THREE.WebGLRenderer();
	renderer.setPixelRatio( window.devicePixelRatio );
	// renderer.setSize( window.innerWidth, CANVAS_HEIGHT );
	renderer.setSize( window.innerWidth, window.innerWidth / 2 );
	container.appendChild( renderer.domElement );

	console.log( renderer.domElement.getContext('webgl2').getExtension('OES_texture_float_linear') );


	// const geometry = new THREE.BoxBufferGeometry( 200, 200, 200 );
	// const material = new THREE.MeshLambertMaterial();
	// const mesh = new THREE.Mesh( geometry, material );
	// scene.add( mesh );

	initPoles();

	initComputeRenderer();

	// stats = new Stats();
	// container.appendChild( stats.dom );

	container.style.touchAction = 'none';
	container.addEventListener( 'pointermove', onPointerMove, false );

	window.addEventListener( 'resize', onWindowResize, false );

	// const gui = new GUI();
	//
	//
	// const effectController = {
	// 	separation: 20.0,
	// 	alignment: 20.0,
	// 	cohesion: 20.0,
	// 	freedom: 0.75
	// };
	//
	// const valuesChanger = function () {
	//
	// 	velocityUniforms[ "separationDistance" ].value = effectController.separation;
	// 	velocityUniforms[ "alignmentDistance" ].value = effectController.alignment;
	// 	velocityUniforms[ "cohesionDistance" ].value = effectController.cohesion;
	// 	velocityUniforms[ "freedomFactor" ].value = effectController.freedom;
	//
	// };
	//
	// valuesChanger();
	//
	// gui.add( effectController, "separation", 0.0, 100.0, 1.0 ).onChange( valuesChanger );
	// gui.add( effectController, "alignment", 0.0, 100, 0.001 ).onChange( valuesChanger );
	// gui.add( effectController, "cohesion", 0.0, 100, 0.025 ).onChange( valuesChanger );
	// gui.close();

	initBirds();

}

function initComputeRenderer() {

	gpuCompute = new GPUComputationRenderer( WIDTH, WIDTH, renderer );

	if ( isSafari() ) {

		gpuCompute.setDataType( THREE.HalfFloatType );

	}

	// create texture and fill
	const dtPosition = gpuCompute.createTexture();
	const dtVelocity = gpuCompute.createTexture();
	fillPositionTexture( dtPosition );
	fillVelocityTexture( dtVelocity );



	// dtGoal = gpuCompute.createTexture();
	// fillGoalTexture( dtGoal );
	// goalVariable = gpuCompute.addVariable( "textureGoal", fragmentShaderGoal, dtGoal );
	// gpuCompute.setVariableDependencies( goalVariable, [ goalVariable ] );

	// bind shader and texture to variable
	velocityVariable = gpuCompute.addVariable( "textureVelocity", fragmentShaderVelocity, dtVelocity );
	positionVariable = gpuCompute.addVariable( "texturePosition", fragmentShaderPosition, dtPosition );

	gpuCompute.setVariableDependencies( velocityVariable, [ positionVariable, velocityVariable ] );
	gpuCompute.setVariableDependencies( positionVariable, [ positionVariable, velocityVariable ] );

	positionUniforms = positionVariable.material.uniforms;
	velocityUniforms = velocityVariable.material.uniforms;

	positionUniforms[ "time" ] = { value: 0.0 };
	positionUniforms[ "delta" ] = { value: 0.0 };
	velocityUniforms[ "time" ] = { value: 1.0 };
	velocityUniforms[ "delta" ] = { value: 0.0 };
	velocityUniforms[ "testing" ] = { value: 1.0 };
	velocityUniforms[ "separationDistance" ] = { value: 20.0 };
	velocityUniforms[ "alignmentDistance" ] = { value: 20.0 };
	velocityUniforms[ "cohesionDistance" ] = { value: 20.0 };
	velocityUniforms[ "freedomFactor" ] = { value: 0.75 };
	velocityUniforms[ "predator" ] = { value: new THREE.Vector3() };

	velocityUniforms[ "hasTarget" ] = { type: "bv", value: Array(BIRDS).fill(0) };
	velocityUniforms[ "targetWire" ] = { type: "iv", value: Array(BIRDS).fill(0) };
	velocityUniforms[ "targets" ] = { type: "fv3", value: Array(3 * BIRDS).fill(0) };

	velocityVariable.material.defines.BOUNDS = BOUNDS.toFixed( 2 );

	velocityVariable.wrapS = THREE.RepeatWrapping;
	velocityVariable.wrapT = THREE.RepeatWrapping;
	positionVariable.wrapS = THREE.RepeatWrapping;
	positionVariable.wrapT = THREE.RepeatWrapping;


	// fillTargetUniform();
	updateTargetUniforms();

	const error = gpuCompute.init();

	if ( error !== null ) {
		console.error( error );
	}

}

function isSafari() {

	return !! navigator.userAgent.match( /Safari/i ) && ! navigator.userAgent.match( /Chrome/i );

}

function initBirds() {

	const geometry = new BirdGeometry();

	// For Vertex and Fragment
	birdUniforms = {
		"color": { value: new THREE.Color( 0xff2200 ) },
		"texturePosition": { value: null },
		"textureVelocity": { value: null },
		// "textureGoal": { value: null },
		"time": { value: 1.0 },
		"delta": { value: 0.0 }
	};

	// THREE.ShaderMaterial
	const material = new THREE.ShaderMaterial( {
		uniforms: birdUniforms,
		vertexShader: birdVS,
		fragmentShader: birdFS,
		side: THREE.DoubleSide

	} );

	const birdMesh = new THREE.Mesh( geometry, material );
	birdMesh.rotation.y = Math.PI / 2;
	birdMesh.matrixAutoUpdate = false;
	birdMesh.updateMatrix();

	scene.add( birdMesh );

}







//
// function updateWire(wire) {
// 	const [p1, p2] = [wire.startPin, wire.endPin];
// 	let [x1,y1,x2,y2,s] = [p1.position.x, p1.position.y, p2.position.x, p2.position.y, 300];
// 	let [a,b,c] = catenary(x1,y1,x2,y2,s);
//
// 	const points = [];
// 	const step = 5;
// 	for(let x=x1; x<x2+step; x+=step) {
// 		x = Math.min(x, x2);
// 		let y = a * Math.cosh((x - b)/a) + c
// 		points.push( new THREE.Vector3(x,y,0) );
// 	}
// 	// let y = a * Math.cosh((x2 - b)/a) + c
// 	// points.push( new THREE.Vector3(x2,y,0) );
//
// 	wire.setPoints(points);
// };
//
// function createWire(p1, p2) {
//
// }



function initPoles() {



	// create pins
	{
		const geometry = new THREE.PlaneGeometry( POLE_WIDTH, POLE_HEIGHT );
		const material = new THREE.MeshBasicMaterial( {color: 0x170f0c} );

		for(let i=0; i<numPins; i++) {
			const mesh = new THREE.Mesh( geometry, material );
			let x = lerp(i/(numPins-1), -1.25 * BOUNDS_HALF, 1.25 * BOUNDS_HALF);
			let y = getRandomFloat(-POLE_HEIGHT/2-120,-POLE_HEIGHT/2+120);
			mesh.position.set(x, y);
			mesh.userData['index'] = i;

			scene.add( mesh );
			pins.push( mesh );
		}
	}

	// create wires
	{
		for(let i=0; i<numPins-1; i++) {
			// const line = new MeshLine();
			// const material = new MeshLineMaterial( { color: 0x000000, lineWidth: 3.0 } );
			// const mesh = new THREE.Mesh(line, material);
			let [p1, p2] = [pins[i], pins[i+1]];
			let wire = new Wire(
				p1.position.x+POLE_WIDTH/2-2,
				p1.position.y+POLE_HEIGHT/2-2,
				p2.position.x-POLE_WIDTH/2+2,
				p2.position.y+POLE_HEIGHT/2-2,
				WIRE_LENGTH);
			wires.push(wire);
			scene.add( wire.mesh );
		}
	}



	control = new DragControls( pins, camera, renderer.domElement );

	control.addEventListener( 'dragstart', (event) => {
		const hasTarget = velocityUniforms['hasTarget'].value;
		const targetWire = velocityUniforms['targetWire'].value;
		const targets = velocityUniforms['targets'].value;

		const mesh = event.object;
		const pinIdx = mesh.userData.index;

		for(let i=0; i < BIRDS; i++) {
			const wireIdx = targetWire[i];

			if (wireIdx == (pinIdx - 1) || wireIdx == pinIdx) {
				hasTarget[i] = false;
			}
		}
	});

	control.addEventListener( 'dragend', (event) => {
		updateTargetUniforms();
	});

	control.addEventListener( 'drag', function ( event ) {
		// get pole index
		const mesh = event.object;
		const i = mesh.userData.index;

		// clamp to x pixel between adjacent pins
		let x = clamp(pins[i-1].position.x + 1, mesh.position.x, pins[i+1].position.x - 1);
		mesh.position.set(x, mesh.position.y);

		// update wires connected to moved pin
		function updateWire(wire, p1, p2) {
			wire.update(
				p1.position.x+POLE_WIDTH/2-2,
				p1.position.y+POLE_HEIGHT/2-2,
				p2.position.x-POLE_WIDTH/2+2,
				p2.position.y+POLE_HEIGHT/2-2,
				WIRE_LENGTH);
		};
		updateWire(wires[i-1], pins[i-1], pins[i]);
		updateWire(wires[i], pins[i], pins[i+1]);
	});
}




function fillPositionTexture( texture ) {

	const theArray = texture.image.data;

	for ( let k = 0, kl = theArray.length; k < kl; k += 4 ) {

		const x = Math.random() * BOUNDS - BOUNDS_HALF;
		const y = Math.random() * BOUNDS - BOUNDS_HALF;
		const z = Math.random() * BOUNDS - BOUNDS_HALF;

		theArray[ k + 0 ] = x;
		theArray[ k + 1 ] = y;
		theArray[ k + 2 ] = z;
		theArray[ k + 3 ] = 1;

	}

}

function fillVelocityTexture( texture ) {

	const theArray = texture.image.data;

	for ( let k = 0, kl = theArray.length; k < kl; k += 4 ) {

		const x = Math.random() - 0.5;
		const y = Math.random() - 0.5;
		const z = Math.random() - 0.5;

		theArray[ k + 0 ] = x * 10;
		theArray[ k + 1 ] = y * 10;
		theArray[ k + 2 ] = z * 10;
		theArray[ k + 3 ] = 1;

	}

}

// function fillGoalTexture( texture ) {
//
//
// 	const data = texture.image.data;
//
// 	for ( let k = 0, kl = data.length; k < kl; k += 4 ) {
//
// 		const wire = wires[getRandomInt(0, wires.length - 1)];
// 		let [x,y] = wire.getPosition(Math.random());
//
// 		// const x = Math.random() * BOUNDS - BOUNDS_HALF;
// 		// const y = 0;
// 		const z = 0;
//
// 		data[ k + 0 ] = x;
// 		data[ k + 1 ] = y;
// 		data[ k + 2 ] = z;
// 		data[ k + 3 ] = 1;
//
// 		// console.log(x,y,z);
// 	}
//
// }


// update targets where hasTarget is false
function updateTargetUniforms() {
	const hasTarget = velocityUniforms['hasTarget'].value;
	const targetWire = velocityUniforms['targetWire'].value;
	const targets = velocityUniforms['targets'].value;

	for(let i = 0; i < BIRDS; i++) {
		// skip if target already set
		if (hasTarget[i]) continue;

		// default to out of bounds
		let [x, y] = [-1.25 * BOUNDS_HALF, 0];

		// try to find a target for the bird
		let attempt = 0;
		while(attempt < 100) {
			const wireIdx = getRandomInt(0, wires.length - 1);
			const wire = wires[wireIdx];
			const t = Math.random();

			// if not too steep
			if (Math.abs(wire.getSlope(t)) < 0.5) {
				[x,y] = wire.getPosition(t);

				hasTarget[i] = true;
				targetWire[i] = wireIdx;
				targets[ 3*i + 0 ] = x;
				targets[ 3*i + 1 ] = y;
				targets[ 3*i + 2 ] = 0;
				break;
			}

			attempt++;
		}


	}


}


//
// function fillTargetUniform() {
//
// 	const set = new Set(disturbedBirds);
//
// 	const data = velocityUniforms['targets'].value;
// 	for ( let k = 0, kl = data.length; k < kl; k += 3 ) {
//
// 		if (set.size && !set.has(k/3)) continue;
//
// 		// default to out of bounds
// 		let [x, y] = [-1.25 * BOUNDS_HALF, 0];
//
// 		// get previous wire
// 		// let z = 0;
// 		let z = data[ k + 2 ];
//
// 		// if wire indexes specified and prev wire is not one of them
// 		// if (wireIdxs.length && wireIdxs.indexOf(z) == -1) {
// 		// 	continue;
// 		// }
//
// 		let attempts = 0;
// 		while(attempts < 100) {
// 			const wireIdx = getRandomInt(0, wires.length - 1);
// 			const wire = wires[wireIdx];
// 			const t = Math.random();
//
// 			// if valid position (not too steep)
// 			if (Math.abs(wire.getSlope(t)) < 0.5) {
// 				[x,y] = wire.getPosition(t);
//
// 				data[ k + 0 ] = x;
// 				data[ k + 1 ] = y;
// 				data[ k + 2 ] = wireIdx;
// 				break;
// 			}
//
// 			attempts++;
// 		}
//
// 		// const z = 0;
//
// 		// data[ k + 0 ] = x;
// 		// data[ k + 1 ] = y;
// 		// data[ k + 2 ] = z;
//
// 		// console.log(x,y,z);
// 	}
//
// }



function onWindowResize() {

	windowHalfX = window.innerWidth / 2;
	windowHalfY = window.innerWidth / 4;

	camera.aspect = 2;
	camera.updateProjectionMatrix();

	renderer.setSize( window.innerWidth, window.innerWidth / 2  );

}

function onPointerMove( event ) {

	if ( event.isPrimary === false ) return;

	// mouseX = event.clientX - windowHalfX;
	// mouseY = event.clientY - windowHalfY;

	mouseX = event.offsetX - windowHalfX;
	mouseY = event.offsetY - windowHalfY;


}

//

function animate() {

	requestAnimationFrame( animate );

	render();
	// stats.update();

}

function render() {

	const now = performance.now();
	let delta = ( now - last ) / 1000;

	if ( delta > 1 ) delta = 1; // safety cap on large deltas
	last = now;

	positionUniforms[ "time" ].value = now;
	positionUniforms[ "delta" ].value = delta;
	velocityUniforms[ "time" ].value = now;
	velocityUniforms[ "delta" ].value = delta;
	birdUniforms[ "time" ].value = now;
	birdUniforms[ "delta" ].value = delta;

	velocityUniforms[ "predator" ].value.set( 0.5 * mouseX / windowHalfX, - 0.5 * mouseY / windowHalfY, 0 );

	mouseX = 10000;
	mouseY = 10000;

	gpuCompute.compute();

	birdUniforms[ "texturePosition" ].value = gpuCompute.getCurrentRenderTarget( positionVariable ).texture;
	birdUniforms[ "textureVelocity" ].value = gpuCompute.getCurrentRenderTarget( velocityVariable ).texture;
	// birdUniforms[ "textureGoal" ].value = gpuCompute.getCurrentRenderTarget( goalVariable ).texture;


	renderer.render( scene, camera );

}

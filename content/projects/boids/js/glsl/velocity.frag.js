export default `

const int WIDTH = 8;
const int BIRDS = WIDTH * WIDTH;

uniform float time;
uniform float testing;
uniform float delta; // about 0.016
uniform float separationDistance; // 20
uniform float alignmentDistance; // 40
uniform float cohesionDistance; //
uniform float freedomFactor;
uniform vec3 predator;

uniform bool hasTarget[BIRDS];
uniform vec3 targets[BIRDS];


// gl_FragCoord.y * float(WIDTH) + gl_FragCoord.x
// const int test = int(gl_FragCoord.y);

const float width = resolution.x;
const float height = resolution.y;

const float PI = 3.141592653589793;
const float PI_2 = PI * 2.0;
// const float VISION = PI * 0.55;

float zoneRadius = 40.0;
float zoneRadiusSquared = 1600.0;

float separationThresh = 0.45;
float alignmentThresh = 0.65;

const float UPPER_BOUNDS = BOUNDS;
const float LOWER_BOUNDS = -UPPER_BOUNDS;

const float SPEED_LIMIT = 9.0;

float rand( vec2 co ){
  return fract( sin( dot( co.xy, vec2(12.9898,78.233) ) ) * 43758.5453 );
}

void main() {

  zoneRadius = separationDistance + alignmentDistance + cohesionDistance;
  separationThresh = separationDistance / zoneRadius;
  alignmentThresh = ( separationDistance + alignmentDistance ) / zoneRadius;
  zoneRadiusSquared = zoneRadius * zoneRadius;


  vec2 uv = gl_FragCoord.xy / resolution.xy;
  vec3 birdPosition, birdVelocity;

  vec3 selfPosition = texture2D( texturePosition, uv ).xyz;
  vec3 selfVelocity = texture2D( textureVelocity, uv ).xyz;
  // vec3 selfGoal = texture2D( textureGoal, uv ).xyz;

  float dist;
  vec3 dir; // direction
  float distSquared;

  float separationSquared = separationDistance * separationDistance;
  float cohesionSquared = cohesionDistance * cohesionDistance;

  float f;
  float percent;

  vec3 velocity = selfVelocity;

  float limit = SPEED_LIMIT;

  // // Preditor distance
  // dir = predator * UPPER_BOUNDS - selfPosition;
  // dir.z = 0.;
  // // dir.z *= 0.6;
  // dist = length( dir );
  // distSquared = dist * dist;
  //
  // float preyRadius = 150.0;
  // float preyRadiusSq = preyRadius * preyRadius;
  //
  // // move birds away if within prey radious of preditor
  // bool predatorNear = false;
  // if ( dist < preyRadius ) {
  //   predatorNear = true;
  //   f = ( distSquared / preyRadiusSq - 1.0 ) * delta * 100.;
  //   velocity += normalize( dir ) * f;
  //   limit += 5.0;
  // }

  // // original
  // // Attract flocks to the center
	// vec3 central = vec3( 0., 0., 0. );
	// dir = selfPosition - central;
	// dist = length( dir );
  //
	// dir.y *= 2.5;
	// velocity -= normalize( dir ) * delta * 5.;


  // if (testing == 0.0) {}
  // if ( rand( uv + time ) < freedomFactor ) {}


  // Attract flocks to the center
  // vec3 central = vec3( 0., 0., 0. );
  // dir = selfPosition - central;
  // dist = length( dir );
  // dir.y *= 2.5;

  // // index of bird this frag shader is executing on
  // int birdIdx = int(gl_FragCoord.y * float(WIDTH) + gl_FragCoord.x);
  // vec3 target = targets[birdIdx];
  // dir = selfPosition - target;
  // float distToTarget = length(dir);
  //
  // dist = length( dir );
  //
  // // otherwise adjust velocity towards target based on distance
  // velocity -= normalize( dir ) * delta * 0.1 * clamp(dist, 1., 100.);

  // accumulate velocity influance from all other birds
  // for every point (bird) in the texture (rbgw represent pos and phase)
  for ( float y = 0.0; y < height; y++ ) {
    for ( float x = 0.0; x < width; x++ ) {

      // dir and dist between bird and self
      vec2 ref = vec2( x + 0.5, y + 0.5 ) / resolution.xy;
      birdPosition = texture2D( texturePosition, ref ).xyz;
      dir = birdPosition - selfPosition;
      dist = length( dir );

      // skip self
      if ( dist < 0.0001 ) continue;

      // skip if bird is too far away from self
      distSquared = dist * dist;
      if ( distSquared > zoneRadiusSquared ) continue;

      percent = distSquared / zoneRadiusSquared;



      if ( percent < separationThresh ) { // low
        // Separation - Move apart for comfort
        // f: large -> 0 as percent approaches separationThresh
        f = ( separationThresh / percent - 1.0 ) * delta;
        velocity -= normalize( dir ) * f;

      } else if ( percent < alignmentThresh ) { // high
        // Alignment - fly the same direction
        // adjustedPercent: 0 -> 1 as percent approaches alignmentThresh
        float threshDelta = alignmentThresh - separationThresh;
        float adjustedPercent = ( percent - separationThresh ) / threshDelta;
        birdVelocity = texture2D( textureVelocity, ref ).xyz;
        f = ( 0.5 - cos( adjustedPercent * PI_2 ) * 0.5 + 0.5 ) * delta;
        velocity += normalize( birdVelocity ) * f;

      } else {

        // Attraction / Cohesion - move closer
        float threshDelta = 1.0 - alignmentThresh;
        float adjustedPercent;
        if( threshDelta == 0. )
          adjustedPercent = 1.;
        else
          adjustedPercent = ( percent - alignmentThresh ) / threshDelta;

        f = ( 0.5 - ( cos( adjustedPercent * PI_2 ) * -0.5 + 0.5 ) ) * delta;

        velocity += normalize( dir ) * f;

      }

    }

  }


  // Preditor distance
  dir = predator * UPPER_BOUNDS - selfPosition;
  dir.z = 0.;
  // dir.z *= 0.6;
  dist = length( dir );
  distSquared = dist * dist;

  float preyRadius = 150.0;
  float preyRadiusSq = preyRadius * preyRadius;

  // if within radius of preditor
  if ( dist < preyRadius ) {
    f = ( distSquared / preyRadiusSq - 1.0 ) * delta * 100.;
    velocity += normalize( dir ) * f;
    limit += 5.0;

  // otherwise consider target
  } else {
    // index of bird this frag shader is executing on
    int birdIdx = int(gl_FragCoord.y * float(WIDTH) + gl_FragCoord.x);

    // inefficient hack to allow array indexing via a non-const
    // on some webgl implementations (specifically mobile)
    for(int i=0; i<BIRDS; i++) {
      if (i != birdIdx) continue;

      if (hasTarget[i]) {
        vec3 target = targets[i];
        dir = selfPosition - target;
        dist = length( dir );
        // otherwise adjust velocity towards target based on distance
        velocity -= normalize( dir ) * delta  * clamp(dist, 0.0, 10.0);

        // vec3 direct = -normalize(dir) * delta * min(dist*dist, 400.);
        vec3 direct = -normalize(dir) * delta * dist * dist;

        // lerp to more direct behavior based on distance to target
        velocity = mix(direct, velocity, min(dist, 20.0)/20.0);

      }


    }




  }





  // this make tends to fly around than down or up
  // if (velocity.y > 0.) velocity.y *= (1. - 0.2 * delta);

  // Speed Limits
  if ( length( velocity ) > limit ) {
    velocity = normalize( velocity ) * limit;
  }

  gl_FragColor = vec4( velocity, 1.0 );

}

`;

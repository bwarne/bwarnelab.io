---
title: N-Sided Die
date: 2015-03-08
thumbnail: /projects/n-die/img/print-polish.jpg
hideMeta: true
---

An OpenSCAD script to generate a n-sided die model for 3d printing.  Source available on [github](https://github.com/bwarne/n-die).  View and generate models on [Thingiverse](https://www.thingiverse.com/thing:58408).

<div class="row" style="display: flex;">
  <div class="col">
    <img src="img/print-rough.jpg" alt="rough" style="flex: 1; padding: 5px;">
  </div>
  <div class="col">
    <img src="img/print-polish.jpg" alt="polish" style="flex: 1; padding: 5px;">
  </div>
</div>

Sure, six choices are no match for a standard die. But what about picking from seven, eleven, or the unthinkable twenty-three choices? Until now, you were never able to leave it up to chance, forcing yourself to go through the agonizing processes of choosing.

Well here comes the solution to all your multi-optioned problems - a customizable die for all your random number needs! You may have ninety-nine problems, but now choosing ain't one.


<div class="row" style="display: flex; font-size: small;">
  <div class="col">
    <img src="img/d7.jpg" alt="d7" style="flex: 1; padding: 5px;">
    <center>d7</center>
  </div>
  <div class="col">
    <img src="img/d11.jpg" alt="d11" style="flex: 1; padding: 5px;">
    <center>d11</center>
  </div>
  <div class="col">
    <img src="img/d23.jpg" alt="d23" style="flex: 1; padding: 5px;">
    <center>d23</center>
  </div>
</div>


The die is designed to have a variable number of flat faces with a number on the opposite side. Wherever the die comes to rest, the associated number will show face up for easy reading.

Use the OpenSCAD script parameters to modify number of sides, font and face depth, style and size of the font, as well as the model resolution. The procedural model uses the golden ratio to construct reasonably even distribution of faces on a sphere for most values of n. Some smaller values of n use preset face angles. Due to most distributions being non-symmetrical, numbers will generally appear off-center of the flat faces in the die.

For best results of the lower hemisphere of the die, print using a raft and supports. Finish the bottom and sides using fine grain sandpaper.

DISCLAIMER: The distribution algorithm works well for large values of n, but it's not perfect. Other variables introduced while printing or sanding pretty much guarantee that it won't result in a perfectly fair die, but it's close!
